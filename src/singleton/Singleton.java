package singleton;


public class Singleton {

 
    public static void main(String[] args) {
        VariasInstancias v1 = new VariasInstancias();
        VariasInstancias v2 = new VariasInstancias();
        System.out.println(v1.getQuant());
        UmaInstancia u1= UmaInstancia.obter();
        UmaInstancia u2= UmaInstancia.obter();
        System.out.println(u2.getQuant());
    }
    
}
