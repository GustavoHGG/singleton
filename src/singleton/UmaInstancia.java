package singleton;

public class UmaInstancia {
    public static int c;
    public static UmaInstancia u;
    public UmaInstancia(){
        c++;
    }
    public static UmaInstancia obter(){
        if(u==null) u= new UmaInstancia();
        return u;
    }
    public static int getQuant(){
        return c;
    }
}
